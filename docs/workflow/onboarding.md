---
title: Onboarding new starters
---

The following checklist is to be used as a reminder of the admin tasks needed to onboard a new member to the DevOps team. These should be done after the University's onboarding tasks for any new member of staff.

### Performed by manager or admin staff before start date

* Add to the corresponding [DevOps subgroup](https://www.lookup.cam.ac.uk/group/uis-devops-division) within DevOps Division lookup group
* Add to the UIS group and any other relevant groups on MS Teams
* Add to the regular meetings they are expected to attend

### Performed by manager or peer before start date

#### Gitlab

* Add to [GitLab group](https://gitlab.developers.cam.ac.uk/groups/uis/devops/-/group_members)
* Add as a viewer or admin as appropriate for [Google Cloud resources](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-infra/)
* Add to https://gitlab.developers.cam.ac.uk/uis/devops/docs/general/ so the new starter can follow the onboarding instructions

#### Google

Add to the relevant Google Shared Drives:

  * [Google Shared drive for DevOps](https://drive.google.com/drive/folders/0AJboYXmrsklAUk9PVA)

#### Other services

* Invite to 1password and add to appropriate vaults.

### Performed by the new starter

Follow the instructions for the first time setup of [Raven](https://raven.cam.ac.uk/).

#### Microsoft

* Activate your Microsoft account, it may take some hours before it is ready
* Outlook
* Calendar

#### Mailing lists

Subscribe to appropriate lists by searching for them in [sympa search](https://lists.cam.ac.uk/sympa/search_list_request). Note that with some you will need to wait for approval.

* uis-service-automation
* uis-staff
* uis-individual-staff
* social (various, optional)

#### SSH key

* Create a 4096 bit RSA key if you don't already have a preferred one
* Add your SSH public key to gitlab to allow for git over SSH
* Open Merge Requests as appropriate to add your key to the various resources

  * [team-data](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/team-data/-/blob/master/team_data.json) where the meaning of admin, deploy and view can be found at ...
  * [ansible-roles](https://gitlab.developers.cam.ac.uk/uis/devops/infra/ansible-roles/tree/master/roles/devops-ssh-users) which will only be needed if you are dealing with legacy systems

#### GPG

You may want to at least:

  * Open a Merge Request in the [team-data project](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/team-data/-/blob/master/team_data.json) on GitLab adding yourself as a DevOps team deploy user.
  * Re-encrypt ansible vault passwords, see [setting up GPG to decrypt secrets](https://gitlab.developers.cam.ac.uk/uis/devops/docs/general/blob/master/docs/secrets.md#setting-up-gpg-to-decrypt-secrets) for guidance
  * Add public GPG key to [team public keyring](https://gitlab.developers.cam.ac.uk/uis/devops/docs/general/blob/master/files/teampubkeys.gpg)
  * Update the guidebook - [copy the updated file](https://gitlab.developers.cam.ac.uk/uis/devops/docs/general/-/blob/master/docs/secrets.md#updating-the-publicly-available-keyring)

There are more detailed instructions in the [general docs](https://gitlab.developers.cam.ac.uk/uis/devops/docs/general/blob/master/docs/secrets.md) to add your GPG key.

#### Documentation

* [guidebook](https://guidebook.devops.uis.cam.ac.uk/en/latest/)
* [general search](https://search.cam.ac.uk/)
* [lookup people](https://www.lookup.cam.ac.uk/)
