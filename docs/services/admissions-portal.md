# Admissions Portal

This page gives an overview of the Admissions Portal, describing its current status, where and
how it's developed and deployed, and who is responsible for maintaining it.

!!! danger
    This service is currently pre-alpha and is **not usable in production**.

!!! note
    This service only applies to undergraduate applicants. The
    [Applicant Portal for postgraduates](https://apply.postgraduate.study.cam.ac.uk/) is an
    unrelated web application.

## Service Description

The Admissions Portal service provides a web application for undergraduate applicants to supply
supplementary information as part of their university application, and to track the progress of
their university application.

## Service Status

The Admissions Portal is currently pre-alpha.

## Contact

Technical queries and support should be directed to
[Dave Hart](https://www.lookup.cam.ac.uk/person/crsid/dkh21) and will be picked up by a member of
the team working on the service. To ensure that you receive a response, always direct requests to
[Dave Hart](https://www.lookup.cam.ac.uk/person/crsid/dkh21) rather than reaching out to team
members directly.

Issues discovered in the service or new feature requests should be opened as GitLab issues
[here](https://gitlab.developers.cam.ac.uk/uis/devops/digital-admissions/operational-support-and-helpdesk).

## Environments

The Admissions Portal is currently deployed to the following environments:

| Name        | Public Application URL | Service-to-Service Backend API URL |
| ----------- | ---------------------- | ---------------------------------- |
| Production  | n/a | n/a |
| Staging     | [https://webapp.test.ap.gcp.uis.cam.ac.uk/](https://webapp.test.ap.gcp.uis.cam.ac.uk/) | [https://apiapp.test.ap.gcp.uis.cam.ac.uk/](https://apiapp.test.ap.gcp.uis.cam.ac.uk/) |
| Development | [https://webapp.devel.ap.gcp.uis.cam.ac.uk/](https://webapp.devel.ap.gcp.uis.cam.ac.uk/) | [https://apiapp.devel.ap.gcp.uis.cam.ac.uk/](https://apiapp.devel.ap.gcp.uis.cam.ac.uk/) |

The GCP console pages for managing the infrastructure of each component of the deployment are:

| Name        | Public Application Static Assets | Public Application Backend | Service-to-Service Backend API |
| ----------- | -------------------------------- | -------------------------- | ------------------------------ |
| Production  | n/a | n/a | n/a |
| Staging     | [GCP CDN](https://console.cloud.google.com/net-services/cdn/list?project=ap-test-3d41aff5) | [Firebase](https://console.firebase.google.com/project/ap-test-3d41aff5) | [GCP Cloud Run](https://console.cloud.google.com/run?project=ap-test-3d41aff5) |
| Development | [GCP CDN](https://console.cloud.google.com/net-services/cdn/list?project=ap-devel-dff67399) | [Firebase](https://console.firebase.google.com/project/ap-devel-dff67399) | [GCP Cloud Run](https://console.cloud.google.com/run?project=ap-devel-dff67399) |

All environments share access to a set of secrets stored in the [meta-project Secret Manager](https://console.cloud.google.com/security/secret-manager?project=ap-meta-296bac5a).

## Source code

The source code for the Admissions Portal is spread over the following repositories:

| Repository  | Description
| ----------- | ------------------ |
| [Frontend](https://gitlab.developers.cam.ac.uk/uis/devops/digital-admissions/admissions-portal/ap-frontend) | The source code for the application frontend |
| [Service-to-Service Backend API](https://gitlab.developers.cam.ac.uk/uis/devops/digital-admissions/admissions-portal/ap-backend) | The source code for the application service-to-service backend API |
| [Infrastructure Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/digital-admissions/admissions-portal/ap-deployment) | The Terraform infrastructure code for deploying the application to GCP |

## Technologies used

The following gives an overview of the technologies the Admissions Portal is built on.

| Category | Language | Framework(s) |
| -------- | -------- | --------- |
| Frontend | JavaScript | React 17 |
| Service-to-Service Backend API | Python 3.8 | FastAPI |

## Operational documentation

The following gives an overview of how the Admissions Portal is deployed and maintained.

### How and where the Admissions Portal is deployed

The Admissions Portal infrastucture is deployed using Terraform, with releases of the frontend and
service-to-service backend API deployed by the GitLab CD pipelines associated with the
[infrastructure deployment repository](https://gitlab.developers.cam.ac.uk/uis/devops/digital-admissions/admissions-portal/ap-deployment).

### Deploying a new release

The `README.md` files in each of the source code repositories explain how to deploy the Admissions
Portal.

### Monitoring

[GCP Cloud Monitoring](https://console.cloud.google.com/monitoring?project=ap-meta-296bac5a)
is used for tracking the health of applications in the environments and sending alert emails when
problems are detected.

### Debugging

TBD

### Specifications

- [Admissions Portal: High-Level Specification](https://docs.google.com/document/d/1UJFCdnS06PpmILPfj75T0TTqnLQ7ZNEm-iIZxRVGXKU)
- [Admissions Portal: Backend Specification](https://docs.google.com/document/d/10aHuF78vrxITQkCLsUUUOmiodZOu7mvCI2_dBYbYgpk)
- [Admissions Portal: Frontend Specification](https://docs.google.com/document/d/1TKutBiVlW076sjX-EkM_nOTdOlZvSgssHDb4Y6czKPU)
- [Digital Admissions Project Index](https://docs.google.com/document/d/19j0wYtijlYHv3AYGKnSFSNASCSJT4saFEWFhQc3chgs)

## Service Management and tech lead

The **service owner** for the Admissions Portal is TBD.

The **service manager** for the Admissions Portal is TBD.

The **tech lead** for the Admissions Portal is
[Dave Hart](https://www.lookup.cam.ac.uk/person/crsid/dkh21).

The following engineers have operational experience with the Admissions Portal and are able to
respond to support requests or incidents:

* [Richard Peach](https://www.lookup.cam.ac.uk/person/crsid/rp431)
* [Mike Bamford](https://www.lookup.cam.ac.uk/person/crsid/mb2174)
